from time import time
from random import Random

RANDOM = Random()


def trial_composite(a, t, n, s):
    if abs(pow(a, t, n)) == 1:
        return False
    for i in range(s):
        if pow(a, 2 ** i * t, n) == n - 1:
            return False
    return True


def probably_prime(n: int, iterations_count: int):
    checked_numbers = set()

    s = 0
    t = n - 1
    while t % 2 == 0:
        t >>= 1
        s += 1

    for i in range(iterations_count):
        a = RANDOM.randint(2, n - 2)
        while len(checked_numbers) < n - 3 and a in checked_numbers:
            a = RANDOM.randint(2, n - 2)
        checked_numbers.add(a)

        if trial_composite(a, t, n, s):
            return False

    return True


def main():
    n = int(input('Введите число: '))
    iterations_count = int(input('Введите количество итераций: '))
    if n < 5:
        print('Число должно быть больше 4')
        return
    if n % 2 == 0:
        print('Число составное')
        return

    start_time = time()
    is_probably_prime = probably_prime(n, iterations_count)
    end_time = time()
    print(f'Число {f"простое с вероятностью >= {1 - 1 / pow(4, iterations_count)}" if is_probably_prime else "составное"}')
    print(f'Вычисление заняло {end_time - start_time} секунд')


if __name__ == '__main__':
    main()
