from random import Random
from math import gcd, ceil, log2
from time import time


RANDOM = Random()


def get_trials_count_by_error(error: float):
    return int(ceil(log2(1 / error)))


def probably_prime(n: int, iterations_count: int):
    checked_numbers = set()

    for i in range(iterations_count):
        a = RANDOM.randint(2, n - 2)
        while len(checked_numbers) < n - 3 and a in checked_numbers:
            a = RANDOM.randint(2, n - 2)
        checked_numbers.add(a)

        if gcd(a, n) != 1:
            return False
        if pow(a, n - 1, n) != 1:
            return False

    return True


def main():
    n = int(input('Введите число: '))
    iterations_count = int(input('Введите количество итераций: '))
    if n < 4:
        print('Число должно быть больше 3')
        return
    if n % 2 == 0:
        print('Число составное')
        return

    start_time = time()
    is_probably_prime = probably_prime(n, iterations_count)
    end_time = time()
    print(f'Число {f"простое с вероятностью >= {1 - 1 / pow(2, iterations_count)}" if is_probably_prime else "составное"}')
    print(f'Вычисление заняло {end_time - start_time} секунд')


if __name__ == '__main__':
    main()
